document.getElementById('submit').addEventListener('click', () => {
  axios.post('/api/url-shorten', {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    url: document.getElementById('url').value
  })
      .then(function (response) {
        fetch('https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=' + encodeURI(response.data.result_url))
            .then(function (response) {

              response.blob().then(blobResponse => {
                let urlCreator = window.URL || window.webkitURL;
                let img = document.getElementById('qr-code');
                img.src = urlCreator.createObjectURL(blobResponse);
                document.getElementById('qr-code-block').style.display = 'block';
              })
            })
            .catch(function (error) {
              console.log(error);
            });
        document.getElementById('short-link').innerHTML = response.data.result_url;
        document.getElementById('short-link-block').style.display = 'block';
      })
      .catch(function (error) {
        console.log(error);
      });
});

