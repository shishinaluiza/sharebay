const express = require('express');
const axios = require('axios').default;
const app = express();
const port = process.env.PORT || 8080;

app.set('view engine', 'jade');
app.use(express.json());
app.use(express.static('public/'));

app.get('/', function (req, res) {
  res.render('index');
});


app.post('/api/url-shorten', function (req, res) {
  console.log(req.body.url);
  axios.post('https://cleanuri.com/api/v1/shorten', {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    url: req.body.url
  })
      .then(function (response) {
        res.send(JSON.stringify(response.data));
      })
      .catch(function (error) {
        res.send(JSON.stringify(error));
      });
});

app.listen(port, () => {
  console.log('Starting Webserver on 127.0.0.1:' + port);
});