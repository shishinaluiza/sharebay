# ShareBay
ShareBay is an application, that lets you easily share links. 
ShareBay shortens your Link, validates and generates QR-Code for it.

Shortening the URL also makes a smaller QR-Code possible. Smaller QR-Codes are easier, faster and safer to scan.

## Installation

You'll need Node.js with npm to run this application.

After cloning the repository run: 

```npm install```

and then:

```node app.js```